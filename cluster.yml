# Define your cluster:
variables:
  # The numeric ID of the GitLab project. Doesn't need to be set on GitLab CI as it can be autodetected (via $CI_PROJECT_ID).
  # The project should be a fork of https://gitlab.com/opencraft/dev/grove-template/
  # For example, You can click the "three dots" button at the top right of
  # https://gitlab.com/opencraft/dev/grove-template/ (next to the "fork" button) to see that its project ID is 24377526.
  # The GitLab project ID is also found in "Settings > General"
  GITLAB_PROJECT_NUMERIC_ID: setme
  # GitLab container registry path
  # ex: registry.gitlab.com/foo/bar where foo/bar is the GitLab repo
  # For https://gitlab.com/opencraft/dev/grove-template/ it would be "registry.gitlab.com/opencraft/dev/grove-template"
  CI_REGISTRY_IMAGE: registry.gitlab.com/<GITLAB_PROJECT_PATH>
  # The name of this cluster. Example: "myorg_aws_openedx"
  # (note: you can host multiple Open edX instances on each cluster, so this is not the name of your site.)
  # For DigitalOcean, this name cannot contain any underscore (hyphens are OK)
  # For minikube, this has to be "minikube".
  TF_VAR_cluster_name: grove-demo
  # The provider to use for this cluster. ("aws", "digitalocean" or "minikube")
  TF_VAR_cluster_provider: aws
  # The main domain of your cluster.
  # Subdomains of this domain will be used to set up services such as openfaas and monitoring.
  # This domain needs to point to the DO or AWS load balancer, and a wildcard CNAME for subdomains
  # pointing to the main domain has to be set up (example: "*.mycluster 300 IN CNAME mycluster.grove.com.").
  # Currently not used by the minikube provider.
  TF_VAR_cluster_domain: mycluster.example.com
  # For AWS clusters, specify the AWS region.
  TF_VAR_aws_region: us-east-1
  # For AWS clusters, specify the AWS AMI ID related to the region
  # For Ubuntu AMIs (if you want to use Codejail), use image IDs listed at
  # https://cloud-images.ubuntu.com/aws-eks/
  TF_VAR_ami_id: ami-0eca1b299fdb31f00
  # For DigitalOcean clusters, specify the DigitalOcean region:
  TF_VAR_do_region: sfo3
  # As the cluster auto-scales, what's the max. number of nodes you'll allow?
  # Choose what you're comfortable with based on the # of instances and your scaling/budget needs.
  TF_VAR_max_worker_node_count: 5

  ## AWS RDS related variables
  # The RDS MySQL instance class
  # https://docs.aws.amazon.com/AmazonRDS/latest/UserGuide/Concepts.DBInstanceClass.html
  TF_VAR_rds_instance_class: db.t3.micro
  # RDS minimum storage size
  TF_VAR_rds_min_storage: 10
  # RDS maximum storage size
  TF_VAR_rds_max_storage: 15
  # RDS storage alarm (CloudWatch) settings.
  TF_VAR_rds_storage_alarm_enabled: false
  TF_VAR_rds_storage_alarm_alarm_actions: "[]"

  # MongoDB Settings. Required when using the AWS provider.
  TF_VAR_mongodbatlas_project_id: setme

  # GitLab CI deployment related jobs will only run on the specified branch.
  DEPLOYMENT_BRANCH: main

  # GitLab dependency proxy url. Make sure it ends with a slash
  TUTOR_DOCKER_REGISTRY: gitlab.com/<USERNAME>/<GROUP>/dependency_proxy/containers/

  # A valid email that will be used at the contact email in Let's Encrypt HTTPS certificates.
  # NOTE: It cannot be from the domain - @example.com
  TF_VAR_lets_encrypt_notification_inbox: <contact@your-org.com>

  # Kubernetes dashboard
  TF_VAR_k8s_dashboard_enabled: false
  TF_VAR_k8s_dashboard_host: null

  # Prometheus alerting
  TF_VAR_prometheus_enabled: false
  TF_VAR_additional_prometheus_alerts: ""
  TF_VAR_alertmanager_enabled: false
  TF_VAR_alertmanager_config: "{}"

  # Grafana monitoring
  TF_VAR_grafana_enabled: false
  TF_VAR_grafana_host: null

  # Velero backups
  TF_VAR_velero_enabled: true

  # OpenFAAS functions
  TF_VAR_openfaas_enabled: false
  TF_VAR_openfaas_host: null

  # Shared Elasticsearch
  TF_VAR_shared_elasticsearch_enabled: true
  TF_VAR_elasticsearch_config: "{}"
